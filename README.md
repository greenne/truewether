# TruWeather Web Application #

Developed by [Cinnamon Agency](http://cinnamon.agency/) in HTML, CSS, JavaScript, Angular & Node.js.

### Agency Communication Interface ###

Ivan Kovac - ivan.kovac@cinnamon.agency

### Owner ###

Don Berchoff - don.berchoff@truweathersolutions.com


## SETUP/DEPENDENCIES ##
npm install --save ws