// Angular
import { Component, ViewChild } from '@angular/core';
// Components
import { MyForecastComponent } from './my-forecast/my-forecast.component';
import { LoopComponent } from './map-controls/loop/loop.component';
// Services
import { C2cLightningService } from './lightning-feed/c2c-lightning.service';
import { LightningFeedService } from './lightning-feed/lightning-feed.service';
import { UcarThreddsService } from './services/ucar-thredds.service';
// 3rd party
import { tileLayer, latLng, marker, icon, layerGroup } from 'leaflet';
import * as L from 'leaflet';
import '../../node_modules/leaflet-timedimension/dist/leaflet.timedimension.src.js';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [LightningFeedService, C2cLightningService, UcarThreddsService]
})

export class AppComponent {

  @ViewChild(LoopComponent) loopComp;

  constructor(
    private C2cLightningService: C2cLightningService,
    private UcarThreddsService: UcarThreddsService
  ) { }

  leafletMap: any;
  c2cLayerGroup: any;
  c2gLayerGroup: any;
  natRadarGroup: any;
  natRadarLayers: any;
  natRadarLoopCounter: any;
  natRadarLoop: any;
  satelliteIR: any;
  satelliteVisible: any;
  satelliteEnhanced: any;
  satelliteGroup: any;
  threddsLoopGroup: any;
  customLoop: any;

  natRadarLoopStatus = false;
  hideTimedimensionControl = true;
  currentLightningLayer = 'hide-all';
  currentSatelliteLayer = 'none';

  satLoopOptions = {
    'VIS': {
      acronym: 'VIS',
      layers: 'VIS',
      styles: 'boxfill/greyscale',
      availableTimes: null
    },
    'IR': {
      acronym: 'IR',
      layers: 'IR',
      styles: 'boxfill/rainbow',
      availableTimes: null
    },
    'WV': {
      acronym: 'WV',
      layers: 'IR_WV',
      styles: 'boxfill/greyscale',
      availableTimes: null
    }
  };

  endDate = new Date();

  options = {
    layers: [
      tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWF0aWphbWF0aWphIiwiYSI6ImNqOW9iMXVvNDFjZWkyd250YWt5N293NDgifQ.17Rp_cKDonZArEnkqvDGXg',
        {
          maxZoom: 18,
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          id: 'mapbox.streets',
          accessToken: 'your.mapbox.access.token',
        }),
      // this.tdWmsLayer
    ],
    zoom: 5,
    center: latLng(38.879966, -98.726909),
    zoomControl: false,
    
    // begin leaflet time-dimension code: https://github.com/socib/Leaflet.TimeDimension
    timeDimension: true,
    timeDimensionControl: true,
    timeDimensionOptions: {
      timeInterval: 'PT1H/' + this.endDate.toISOString(), //natRadar  PT1H means duration of 1 hour
      period: 'PT4M', // natRadar // PT4M means duration fo 4 minutes
      currentTime: this.endDate
    },
    timeDimensionControlOptions: {
      autoPlay: false,
      playerOptions: {
        buffer: 10,
        transitionTime: 500,
        loop: true,
      }
    },
    // end leaflet time-dimension code: https://github.com/socib/Leaflet.TimeDimension
  };

  mapReady(map: L.Map) {
    this.initAvailableTimes();

    this.leafletMap = map;
    this.leafletMap.addControl(L.control.zoom({ position: 'bottomright' }));

    this.endDate.setUTCMinutes(0, 0, 0);

    // Lightnings
    this.c2cLayerGroup = L.layerGroup([]).addTo(this.leafletMap);
    this.c2gLayerGroup = L.layerGroup([]).addTo(this.leafletMap);

    this.C2cLightningService.messages.subscribe(
      lightnings => {
        if (this.currentLightningLayer != 'hide-all') {

          lightnings.forEach(lightning => {
            if (lightning.type === 1) { // Cloud to cloud
              this.c2cLayerGroup.addLayer(marker([lightning.latitude, lightning.longitude], {
                icon: new L.DivIcon({
                  className: 'lightning-icon lightning-icon--c2c',
                  html: '<i class="fa fa-bolt" aria-hidden="true"></i>'
                })
              }))
            } else if (lightning.type === 0) { // Cloud to ground
              this.c2gLayerGroup.addLayer(marker([lightning.latitude, lightning.longitude], {
                icon: new L.DivIcon({
                  className: 'lightning-icon lightning-icon--c2g',
                  html: '<i class="fa fa-bolt" aria-hidden="true"></i>'
                })
              }))
            } else {
              // console.log('Unknown lightning type: ', lightning)
            }
          });

        }
      }
    );

    // Range rings
    let rangeRingsGroup = L.layerGroup([]).addTo(this.leafletMap);
    // Range rings - Vegas
    L.circle([36.114647, -115.172813], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([36.114647, -115.172813], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius
    // Range rings - Fort Lauderdale
    L.circle([26.1130, -80.1057], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([26.1130, -80.1057], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius
    // Range rings - Griffis airport
    L.circle([43.2299, -75.4060], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([43.2299, -75.4060], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius
    // Range rings - NY - LGA Airport
    L.circle([40.775979, -73.873699], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([40.775979, -73.873699], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius
    // Range rings - NY - EWR Airport
    L.circle([40.691516, -74.179140], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([40.691516, -74.179140], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius
    // Range rings - NY - EWR Airport
    L.circle([40.642781, -73.780642], { radius: 11265 / 2, color: '#eb3941', weight: 3, fill: false }).addTo(rangeRingsGroup); // 7 mile radius
    L.circle([40.642781, -73.780642], { radius: 24140 / 2, color: '#ff9b2e', weight: 3, fill: false }).addTo(rangeRingsGroup); // 15 mile radius

    // Time dimension
    const wmsUrl = 'https://nowcoast.noaa.gov/arcgis/services/nowcoast/radar_meteo_imagery_nexrad_time/MapServer/WMSServer'
    const radarWMS = L.tileLayer.wms(wmsUrl, {
      layers: '1',
      format: 'image/png',
      transparent: true,
      opacity: 0.6,
      attribution: 'nowCOAST'
    });

    // set the national radar Loop
    this.natRadarLoop = L.timeDimension.layer.wms(radarWMS, {
      updateTimeDimension: false,
      wmsVersion: '1.3.0'
    });

    // Satellites
    this.satelliteGroup = L.layerGroup([]).addTo(this.leafletMap);

    this.satelliteVisible = L.tileLayer.wms(
      'https://mesonet.agron.iastate.edu/cgi-bin/wms/goes/conus_vis.cgi?VERSION=1.1.1&request=GetMap&SERVICE=WMS&&srs=EPSG:4326&format=image/png', {
        maxNativeZoom: 11,
        elevation: 0,
        format: 'image/png',
        layers: 'goes_conus_vis',
        crs: L.CRS.EPSG4326,
        opacity: 0.75,
        transparent: true
      })

    // Catch move and zoom
    // this.leafletMap.on('zoomstart', function(ev) {
    //   console.log(ev);

    //   if (this.customLoop) {
    //     this.stopLoop(this.currentSatelliteLayer);
    //     this.initLoop();
    //   }
    // }.bind(this));

    this.leafletMap.on('moveend', function (ev) {
      if (this.customLoop) {
        this.stopLoop(this.currentSatelliteLayer);
        this.initLoop();
      }
    }.bind(this));
  }

  initAvailableTimes() {
    this.UcarThreddsService.getTimes(this.satLoopOptions.VIS.acronym).subscribe(availableTimes => {
      this.satLoopOptions.VIS.availableTimes = availableTimes;
    });
    this.UcarThreddsService.getTimes(this.satLoopOptions.IR.acronym).subscribe(availableTimes => {
      this.satLoopOptions.IR.availableTimes = availableTimes;

      this.satelliteIR = L.tileLayer.wms(
        'http://thredds.ucar.edu/thredds/wms/satellite/IR/SUPER-NATIONAL_8km/current/' + this.satLoopOptions.IR.availableTimes[0], {
          maxNativeZoom: 11,
          elevation: 0,
          format: 'image/png',
          layers: 'IR',
          opacity: 0.45,
          styles: 'boxfill/rainbow',
          logscale: false,
          numcolorbands: 20,
          colorscalerange: '0,206',
          // width: 1519,
          // height: 807
          // SRS: 'EPSG:4326',
          // transparent: true
          // time: '2017-12-14T08:30:20.000Z'
        })
    });
    this.UcarThreddsService.getTimes(this.satLoopOptions.WV.acronym).subscribe(availableTimes => {
      this.satLoopOptions.WV.availableTimes = availableTimes;

      this.satelliteEnhanced = L.tileLayer.wms(
        'http://thredds.ucar.edu/thredds/wms/satellite/WV/SUPER-NATIONAL_8km/current/' + this.satLoopOptions.WV.availableTimes[0], {
          maxNativeZoom: 11,
          elevation: 0,
          format: 'image/png',
          layers: 'IR_WV',
          opacity: 0.55,
          styles: 'boxfill/greyscale',
          logscale: false,
          numcolorbands: 20,
          colorscalerange: '0,206',
          // SRS: 'EPSG:4326',
          // transparent: true
        })
    });
  }

  loaderVisible = false;
  toggleLoader() {
    this.loaderVisible = !this.loaderVisible;
  }

  showLoader() {
    this.loaderVisible = true;
  }

  hideLoader() {
    this.loaderVisible = false;
  }

  // sattelite loop
  initLoop() {
    this.showLoader();
    this.satelliteGroup.clearLayers();

    switch (this.currentSatelliteLayer) {
      case 'visible':
        this.loadLoop('http://thredds.ucar.edu/thredds/wms', this.satLoopOptions.VIS);
        break;
      case 'ir':
        this.loadLoop('http://thredds.ucar.edu/thredds/wms', this.satLoopOptions.IR);
        break;
      case 'enhanced':
        this.loadLoop('http://thredds.ucar.edu/thredds/wms', this.satLoopOptions.WV);
        break;
      case 'none':
        break;
      default:
        console.log('Error in initLoop()');
    }

  }

  // load sattelite loop
  loadLoop(threddsBaseURL, currentSatParams) {
    this.threddsLoopGroup = L.layerGroup([]).addTo(this.leafletMap);
    let threddsLayers = [];
    var loadedLayers = 0;
    let timesToLoop = currentSatParams.availableTimes.slice(0, 7).reverse();

    timesToLoop.forEach(time => {
      let currentLayer = L.tileLayer.wms(
        threddsBaseURL + '/satellite/' + currentSatParams.acronym + '/SUPER-NATIONAL_8km/current/' + time, {
          maxNativeZoom: 11,
          elevation: 0,
          format: 'image/png',
          layers: currentSatParams.layers,
          opacity: 0,
          styles: currentSatParams.styles,
          logscale: false,
          numcolorbands: 20,
          colorscalerange: '0,206'
          // SRS: 'EPSG:4326',
          // transparent: true
        }).addTo(this.threddsLoopGroup);

      threddsLayers.push(currentLayer);

      currentLayer.on('load', function (ev) {
        loadedLayers++;
        if (loadedLayers === threddsLayers.length) {
          this.startLoop();
          this.hideLoader();
        }
      }.bind(this));
    });
  }

  startLoop() {
    var layersArray = this.threddsLoopGroup.getLayers();

    var loopLayersLength = layersArray.length;
    var lastIndex = loopLayersLength - 1;
    var i = 0;

    this.customLoop = setInterval(function () {
      if (i === 0) {
        layersArray[lastIndex].setOpacity(0);
      } else {
        layersArray[i - 1].setOpacity(0);
      }

      layersArray[i].setOpacity(0.55);

      if (i === lastIndex) {
        i = 0;
      } else {
        i++;
      }
    }, 250);
  }

  stopLoop(currentName) {
    this.threddsLoopGroup.clearLayers();
    this.setSatelliteLayers(currentName);
    clearInterval(this.customLoop);
  }

  // radar loop
  toggleNatRadar() {
    // turn off radar
    if (this.natRadarLoopStatus) {
      this.leafletMap.removeLayer(this.natRadarLoop);
      this.natRadarLoopStatus = false;
      this.hideTimedimensionControl = true;
    }
    // start radar 
    else {
      this.natRadarLoop.addTo(this.leafletMap);
      this.natRadarLoop.setZIndex(9999);
      this.natRadarLoopStatus = true;
      this.hideTimedimensionControl = false;
    }
  }

  setLightningLayers($event) {
    if ($event === 'hide-all') {
      this.leafletMap.removeLayer(this.c2cLayerGroup);
      this.leafletMap.removeLayer(this.c2gLayerGroup);
      this.currentLightningLayer = 'hide-all';
    } else if ($event === 'show-all') {
      if (!this.leafletMap.hasLayer(this.c2gLayerGroup)) {
        this.c2gLayerGroup = L.layerGroup([]).addTo(this.leafletMap);
      }
      if (!this.leafletMap.hasLayer(this.c2cLayerGroup)) {
        this.c2cLayerGroup = L.layerGroup([]).addTo(this.leafletMap);
      }
      this.currentLightningLayer = 'show-all';
    } else if ($event === 'c2g') {
      this.leafletMap.removeLayer(this.c2cLayerGroup);
      if (!this.leafletMap.hasLayer(this.c2gLayerGroup)) {
        this.c2gLayerGroup = L.layerGroup([]).addTo(this.leafletMap);
      }
      this.currentLightningLayer = 'c2g';
    } else if ($event === 'c2c') {
      this.leafletMap.removeLayer(this.c2gLayerGroup);
      if (!this.leafletMap.hasLayer(this.c2cLayerGroup)) {
        this.c2cLayerGroup = L.layerGroup([]).addTo(this.leafletMap);
      }
      this.currentLightningLayer = 'c2c';
    } else {
      console.log('setLightningLayers() unhandled event value');
    }
  }

  setSatelliteLayers($event) {
    if (this.threddsLoopGroup) {
      this.threddsLoopGroup.clearLayers();
      clearInterval(this.customLoop);
    }

    this.currentSatelliteLayer = $event;
    this.satelliteGroup.clearLayers();

    // if (this.loopComp.isLooping) {
    // Show play button when switching to another satellite type
    //   console.log('loopcomp islooping: ', this.loopComp.isLooping);
    //   this.loopComp.isLooping = false;
    // // }

    switch ($event) {
      case 'visible':
        this.satelliteVisible.addTo(this.satelliteGroup);
        break;
      case 'ir':
        this.satelliteIR.addTo(this.satelliteGroup);
        break;
      case 'enhanced':
        this.satelliteEnhanced.addTo(this.satelliteGroup);
        break;
      case 'none':
        break;
      default:
        console.log('setSatelliteLayers() unhandled event value');
    }

  }
}
