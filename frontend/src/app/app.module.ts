// Angular
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
// Components
import { AppComponent } from './app.component';
import { MapControlsComponent } from './map-controls/map-controls.component';
import { MyForecastComponent } from './my-forecast/my-forecast.component';
// 3rd party
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoopComponent } from './map-controls/loop/loop.component';
import { LoaderComponent } from './shared/loader/loader.component';
import { AerisService } from './services/aeris.service';


@NgModule({
  declarations: [
    AppComponent,
    MapControlsComponent,
    MyForecastComponent,
    LoopComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    LeafletModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [AerisService],
  bootstrap: [AppComponent]
})
export class AppModule { }
