import { any } from 'codelyzer/util/function';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { LightningFeedService } from './lightning-feed.service';
import { map } from 'rxjs/operators';

// const DATA_STREAM_URL = 'ws://tourgent.co:8081';
// const DATA_STREAM_URL = 'ws://twsportal.weather.unisys.com:443';
const DATA_STREAM_URL = 'ws://twsportal.weather.unisys.com:8081';
// const DATA_STREAM_URL = 'ws://127.0.0.1:8081';

// const DATA_STREAM_URL = 'ws://127.0.0.1:8080';

@Injectable()
export class C2cLightningService {
  public messages: Subject<any>;

  constructor(wsService: LightningFeedService) {
    this.messages = <Subject<any>>wsService
      .connect(DATA_STREAM_URL)
      .pipe(
        map((response: MessageEvent): any => {
          let data = [];
          let lightningStrings = response.data.replace(new RegExp("}", "g"), "};").split(";");

          lightningStrings.forEach(element => {
            let elementSubstr = element.substr(4);
            let elementLenght = element.length;
            if (elementLenght > 50 && elementSubstr.charAt(0) === "{" && elementSubstr.slice(-1) === "}") {
              data.push(JSON.parse(elementSubstr));
            } else {
              // TODO: Figure out why some elements get cut off.
            }
          });

          return data;
        })
      );

  }
}

