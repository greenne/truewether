import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'loop-controls',
  templateUrl: './loop.component.html',
  styleUrls: ['./loop.component.scss']
})
export class LoopComponent implements OnInit {
  @Output() startCurrentLoop = new EventEmitter<string>();
  @Output() stopCurrentLoop = new EventEmitter<string>();
  isLooping = false;

  constructor() { }

  ngOnInit() {
  }

  startLoop() {
    this.isLooping = true;
    this.startCurrentLoop.next('');
  }

  stopLoop() {
    this.isLooping = false;
    this.stopCurrentLoop.next('');
  }

}
