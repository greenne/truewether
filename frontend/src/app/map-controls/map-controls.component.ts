import { Component, OnInit, Output, EventEmitter, Input, ViewEncapsulation } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-map-controls',
  templateUrl: './map-controls.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./map-controls.component.scss']
})
export class MapControlsComponent implements OnInit {
  @Output() setLightningOptionEvent = new EventEmitter<string>();
  @Output() setSatelliteOptionEvent = new EventEmitter<string>();
  @Output() startLoopEvent = new EventEmitter<string>();
  @Output() toggleNatRadarEvent = new EventEmitter<string>();
  @Input() currentLightningLayer;
  @Input() currentSatelliteLayer;
  @Input() natRadarLoopStatus;

  constructor( private modalService: NgbModal ) { }

  ngOnInit() {

  }

  toggleNatRadar() {
    this.toggleNatRadarEvent.next();
  }

  setLightningOption(option) {
    this.setLightningOptionEvent.next(option);
  }

  setSatelliteOption(option) {
    this.setSatelliteOptionEvent.next(option);
  }
  
  startLoop() {
    this.startLoopEvent.next('');
  }

  open(content) {
    this.modalService.open(content, { windowClass: 'right-modal' });
  }
}
