import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-my-forecast',
  templateUrl: './my-forecast.component.html',
  styleUrls: ['./my-forecast.component.scss']
})
export class MyForecastComponent implements OnInit {
  closeResult: string;

  constructor(private modalService: NgbModal) { }

  // open(content) {
  //   this.modalService.open(content, { windowClass: 'right-modal' });
  // }

  ngOnInit() {
  }
}