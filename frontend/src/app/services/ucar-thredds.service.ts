import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class UcarThreddsService {

  constructor(private http: Http) { }

  getTimes(satelliteType) {
    return this.http.get('http://thredds.ucar.edu/thredds/catalog/satellite/' + satelliteType + '/SUPER-NATIONAL_8km/current/catalog.xml').pipe(
      map((res: any) => {
        let parsedResponseDocument = new (<any>window).DOMParser().parseFromString(res.text(), "application/xml");
        let datasets = parsedResponseDocument.getElementsByTagName("dataset")
        let availableTimes = [];

        for (var i = 1; i < datasets.length; i++) {
          availableTimes.push(datasets[i].getAttribute("name"))
        }

        return availableTimes;
      })
    );

  }
}