import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable()
export class WxtilesService {

  constructor(private http: Http) { }

  getTimes(): Observable<Array<String>> {
    return this.http.get('http://api.wxtiles.com/v0/wxtiles/layer/ncep-mrms-us-reflectivity-dbz/instance/MergedReflectivityQCComposite/times/')
      .pipe(
        map(
          (res: any) => res.json()
        )
      );
  }

  // Gets latest times every 30 seconds
  // getTimes30sec(): Observable<Array<String>> {
  //   return Observable.interval(30000).flatMap((i) => this.http.get('http://api.wxtiles.com/v0/wxtiles/layer/ncep-mrms-us-reflectivity-dbz/instance/MergedReflectivityQCComposite/times/').map(
  //     (res) => res.json()
  //   ));
  // }

}


