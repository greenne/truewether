import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'loader-cube-grid',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  currentProgress = 0;

  progressInterval = setInterval( function () {
    let randomIncrease = Math.floor((Math.random() * 10) + 1);
    
    if (this.currentProgress >= 89) {
      let lastProgress = 89 + randomIncrease;
      if (lastProgress > this.currentProgress) {
        this.currentProgress = lastProgress;
      }
      clearInterval(this.progressInterval);
    } else {
      this.currentProgress = this.currentProgress + randomIncrease;
    }
  }.bind(this), 2000);

  ngOnDestroy() {
    clearInterval(this.progressInterval);
  }
}
