"use strict";

console.log('start --- lightningFeeed.js');
const WebSocket = require('ws');
var net = require('net');
require('events').EventEmitter.prototype._maxListeners = 100;

// 
// READ DATA FROM LIGHTNING FEED
// 
var flashFeedSocket = new net.Socket();
	flashFeedSocket.connect(2324, '184.72.125.75', function() { // '184.72.125.75', '107.23.153.83'
	flashFeedSocket.write('{"p":"32D7A9BF-8BDA-4387-A65D-AF0B41AAD6B7","v":3,"f":1,"t":1}');
	console.log('Flash Feed...');
});

flashFeedSocket.on('close', function() {
	console.log('Connection closed - flash');
});

var pulseFeedSocket = new net.Socket();
	pulseFeedSocket.connect(2324, '184.72.125.75', function() { //'184.72.125.75', '107.23.153.83'
	pulseFeedSocket.write('{"p":"32D7A9BF-8BDA-4387-A65D-AF0B41AAD6B7","v":3,"f":1,"t":2}');
	console.log('Pulse Feed...');
});

pulseFeedSocket.on('close', function() {
	console.log('Connection closed - pulse');
});


// 
// WEBSOCKETS REDIRECT SERVER
// 
const wss = new WebSocket.Server({ port: 8081 });
// const wss = new WebSocket.Server({ port: 443 });
let ws;

function incoming(message) {
	console.log('received: %s', message);
}

function onError(error) {
	console.log('onError() --- lightningFeeed.js');
}

function onData(data) {
	console.log('DATA: \r\n' + data);
	ws.send(data.toString('utf-8'), onError);
}

function connection(ws1) {
	console.log('aaa');
	ws = ws1;
	ws.on('message', incoming);

	pulseFeedSocket.on('data', onData);
	flashFeedSocket.on('data', onData);
}

wss.on('connection', connection);
